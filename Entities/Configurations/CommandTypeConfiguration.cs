﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Entities.Configurations
{
    public class CommandTypeConfiguration : IEntityTypeConfiguration<CommandType>
    {
        public void Configure(EntityTypeBuilder<CommandType> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasData(
                new CommandType() { Id = 'A', Name = "Advance" },
                new CommandType() { Id = 'L', Name = "Left" },
                new CommandType() { Id = 'R', Name = "Right" });
        }
    }
}
