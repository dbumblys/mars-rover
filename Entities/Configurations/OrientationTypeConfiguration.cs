﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Entities.Configurations
{
    public class OrientationTypeConfiguration : IEntityTypeConfiguration<OrientationType>
    {
        public void Configure(EntityTypeBuilder<OrientationType> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasData(
                new OrientationType() { Id = 'N', Name = "North" },
                new OrientationType() { Id = 'E', Name = "East" },
                new OrientationType() { Id = 'S', Name = "South" },
                new OrientationType() { Id = 'W', Name = "West" });
        }
    }
}
