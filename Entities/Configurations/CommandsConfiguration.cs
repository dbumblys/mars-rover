﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Configurations
{
    public class CommandsConfiguration : IEntityTypeConfiguration<Commands>
    {
        public void Configure(EntityTypeBuilder<Commands> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Rover);
        }
    }
}
