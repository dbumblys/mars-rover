﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Configurations
{
    public class RoverConfiguration : IEntityTypeConfiguration<Rover>
    {
        public void Configure(EntityTypeBuilder<Rover> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Square);

            builder.HasOne(x => x.Coordinates);

            builder.HasMany(x => x.Commands);

        }
    }
}
