﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Entities.Configurations;

namespace Entities
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Square> Squares { get; set; }
        public DbSet<Rover> Rovers { get; set; }
        public DbSet<Coordinates> Coordinates { get; set; }
        public DbSet<Commands> Commands { get; set; }
        public DbSet<OrientationType> OrientationTypes { get; set; }
        public DbSet<CommandType> CommandTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CommandsConfiguration());
            modelBuilder.ApplyConfiguration(new CommandTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CoordinatesConfiguration());
            modelBuilder.ApplyConfiguration(new OrientationTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RoverConfiguration());
            modelBuilder.ApplyConfiguration(new SquareConfiguration());
        }
    }
}
