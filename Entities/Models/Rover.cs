﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class Rover
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int CoordinatesId { get; set; }
        public Coordinates Coordinates { get; set; }
        public char OrientationId { get; set; }
        public OrientationType Orientation { get; set; }
        public int SquareId { get; set; }
        public Square Square { get; set; }

        public IList<Commands> Commands { get; set; }
    }
}
