﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.Models
{
    public class CommandType
    {
        [Key]
        public char Id { get; set; }
        public string Name { get; set; }
    }
}
