﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class MoveEnumsToDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Commands_Rovers_RoverId",
                table: "Commands");

            migrationBuilder.DropForeignKey(
                name: "FK_Rovers_Coordinates_InitialCoordinatesId",
                table: "Rovers");

            migrationBuilder.DropForeignKey(
                name: "FK_Rovers_Squares_SquareId",
                table: "Rovers");

            migrationBuilder.DropIndex(
                name: "IX_Rovers_InitialCoordinatesId",
                table: "Rovers");

            migrationBuilder.DropColumn(
                name: "InitialCoordinatesId",
                table: "Rovers");

            migrationBuilder.DropColumn(
                name: "InitialOrientation",
                table: "Rovers");

            migrationBuilder.AlterColumn<int>(
                name: "SquareId",
                table: "Rovers",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CoordinatesId",
                table: "Rovers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "OrientationId",
                table: "Rovers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "RoverId",
                table: "Commands",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "CommandTypes",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommandTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrientationTypes",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrientationTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Rovers_CoordinatesId",
                table: "Rovers",
                column: "CoordinatesId");

            migrationBuilder.CreateIndex(
                name: "IX_Rovers_OrientationId",
                table: "Rovers",
                column: "OrientationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Commands_Rovers_RoverId",
                table: "Commands",
                column: "RoverId",
                principalTable: "Rovers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rovers_Coordinates_CoordinatesId",
                table: "Rovers",
                column: "CoordinatesId",
                principalTable: "Coordinates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rovers_OrientationTypes_OrientationId",
                table: "Rovers",
                column: "OrientationId",
                principalTable: "OrientationTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rovers_Squares_SquareId",
                table: "Rovers",
                column: "SquareId",
                principalTable: "Squares",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Commands_Rovers_RoverId",
                table: "Commands");

            migrationBuilder.DropForeignKey(
                name: "FK_Rovers_Coordinates_CoordinatesId",
                table: "Rovers");

            migrationBuilder.DropForeignKey(
                name: "FK_Rovers_OrientationTypes_OrientationId",
                table: "Rovers");

            migrationBuilder.DropForeignKey(
                name: "FK_Rovers_Squares_SquareId",
                table: "Rovers");

            migrationBuilder.DropTable(
                name: "CommandTypes");

            migrationBuilder.DropTable(
                name: "OrientationTypes");

            migrationBuilder.DropIndex(
                name: "IX_Rovers_CoordinatesId",
                table: "Rovers");

            migrationBuilder.DropIndex(
                name: "IX_Rovers_OrientationId",
                table: "Rovers");

            migrationBuilder.DropColumn(
                name: "CoordinatesId",
                table: "Rovers");

            migrationBuilder.DropColumn(
                name: "OrientationId",
                table: "Rovers");

            migrationBuilder.AlterColumn<int>(
                name: "SquareId",
                table: "Rovers",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "InitialCoordinatesId",
                table: "Rovers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InitialOrientation",
                table: "Rovers",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "RoverId",
                table: "Commands",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_Rovers_InitialCoordinatesId",
                table: "Rovers",
                column: "InitialCoordinatesId");

            migrationBuilder.AddForeignKey(
                name: "FK_Commands_Rovers_RoverId",
                table: "Commands",
                column: "RoverId",
                principalTable: "Rovers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rovers_Coordinates_InitialCoordinatesId",
                table: "Rovers",
                column: "InitialCoordinatesId",
                principalTable: "Coordinates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rovers_Squares_SquareId",
                table: "Rovers",
                column: "SquareId",
                principalTable: "Squares",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
