﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class AddEnumsSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "CommandTypes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { "A", "Advance" },
                    { "L", "Left" },
                    { "R", "Right" }
                });

            migrationBuilder.InsertData(
                table: "OrientationTypes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { "N", "North" },
                    { "E", "East" },
                    { "S", "South" },
                    { "W", "West" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommandTypes",
                keyColumn: "Id",
                keyValue: "A");

            migrationBuilder.DeleteData(
                table: "CommandTypes",
                keyColumn: "Id",
                keyValue: "L");

            migrationBuilder.DeleteData(
                table: "CommandTypes",
                keyColumn: "Id",
                keyValue: "R");

            migrationBuilder.DeleteData(
                table: "OrientationTypes",
                keyColumn: "Id",
                keyValue: "E");

            migrationBuilder.DeleteData(
                table: "OrientationTypes",
                keyColumn: "Id",
                keyValue: "N");

            migrationBuilder.DeleteData(
                table: "OrientationTypes",
                keyColumn: "Id",
                keyValue: "S");

            migrationBuilder.DeleteData(
                table: "OrientationTypes",
                keyColumn: "Id",
                keyValue: "W");
        }
    }
}
