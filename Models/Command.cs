﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public enum Command
    {
        Advance = 'A',
        Left = 'L',
        Right = 'R'
    }
}
