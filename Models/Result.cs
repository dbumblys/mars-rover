﻿namespace Models
{
    public class ResultDto
    {
        public bool IsValid { get; set; }
        public char Orientation { get; set; }
        public CoordinatesDto Coordinates { get; set; }
    }
}
