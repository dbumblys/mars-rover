﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public enum Orientation
    {
        North = 'N',
        East = 'E',
        South = 'S',
        West = 'W'
    }
}
