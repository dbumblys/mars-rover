﻿namespace Models
{
    public class CoordinatesDto
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
