﻿namespace Models
{
    public class RoverDto
    {
        public int Id { get; set; }
        public CoordinatesDto Coordinates { get; set; }
        public Orientation Orientation { get; set; }
        public SquareDto Square { get; set; }
        public int SquareId { get; set; }
        public string Commands { get; set; }
    }

    public class RoverCreateDto
    {
        public CoordinatesDto Coordinates { get; set; }
        public char OrientationId { get; set; }
    }
}
