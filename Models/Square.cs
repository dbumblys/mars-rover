﻿namespace Models
{
    public class SquareDto
    {
        public int Id { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public class SquareCreateDto
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
