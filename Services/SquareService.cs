﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Models;

namespace Services
{
    public class SquareService
    {
        private readonly IRepositoryWrapper _repository;

        public SquareService(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        public async Task<Square> CreateSquare(SquareCreateDto square)
        {
            var entity = new Square() { Width = square.Width, Height = square.Height };
            await _repository.Square.Add(entity);

            return entity;
        }

        public async Task<Square> GetSquare(int id)
        {
            var entity = await _repository.Square.FirstOrDefault(x => x.Id == id);

            return entity;
        }

        public async Task<List<Square>> GetAllSquares()
        {
            var entity = await _repository.Square.GetAll();

            return entity.ToList();
        }

        public async Task<Square> UpdateSquare(Square entity, int height, int width)
        {
            entity.Width = width;
            entity.Height = height;

            await _repository.Square.Update(entity);

            return entity;
        }

        public async Task DeleteSquare(Square entity)
        {
            await _repository.Square.Remove(entity);
        }
    }
}
