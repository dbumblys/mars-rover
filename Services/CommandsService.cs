﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Models;

namespace Services
{
    public class CommandsService
    {
        private readonly IRepositoryWrapper _repository;

        public CommandsService(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        public async Task<Commands> CreateCommand(CommandsDto commands, int roverId)
        {
            var entity = new Commands() { RoverId = roverId, RoverCommands = commands.Commands };

            await _repository.Commands.Add(entity);

            return entity;
        }
    }
}
