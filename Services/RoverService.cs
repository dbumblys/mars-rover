﻿using System;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Models;

namespace Services
{
    public class RoverService
    {
        private readonly IRepositoryWrapper _repository;

        public RoverService(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        public async Task<Rover> CreateRover(RoverCreateDto rover, int squareId)
        {
            var entity = new Rover()
            {
                Coordinates = new Coordinates()
                {
                    X = rover.Coordinates.X,
                    Y = rover.Coordinates.Y
                },
                OrientationId = rover.OrientationId,
                SquareId = squareId
            };

            await _repository.Rover.Add(entity);

            return entity;
        }

        public async Task<Rover> GetRover(int id)
        {
            var entity = await _repository.Rover.FirstOrDefault(x => x.Id == id);

            return entity;
        }

        public async Task<Rover> GetRoverWithChilds(int id)
        {
            var entity = await _repository.Rover.GetWhere(x => x.Id == id,
                y => y.Include(xy => xy.Coordinates)
                    .Include(o => o.Orientation)
                    .Include(s => s.Square)
                    .Include(c => c.Commands));

            return entity;
        }

        public ResultDto ExecuteRoverCommands(string commands, CoordinatesDto roverCoordinates, Orientation roverOrientation, int squareWidth, int squareHeight)
        {
            foreach (var command in commands)
            {
                switch (command)
                {
                    case (char)Command.Left:
                        roverOrientation = TurnRoverLeft(roverOrientation);
                        break;
                    case (char)Command.Right:
                        roverOrientation = TurnRoverRight(roverOrientation);
                        break;
                    case (char)Command.Advance:
                        roverCoordinates = RoverAdvance(roverCoordinates, roverOrientation);
                        break;
                    default:
                        throw new NotSupportedException("Invalid command type");
                }
            }

            var result = new ResultDto()
            {
                Coordinates = roverCoordinates,
                IsValid = IsRoverStillInSquare(roverCoordinates, squareWidth, squareHeight),
                Orientation = (char) roverOrientation
            };

            return result;
        }

        private Orientation TurnRoverLeft(Orientation roverOrientation)
        {
            switch (roverOrientation)
            {
                case Orientation.North:
                    return Orientation.West;
                case Orientation.West:
                    return Orientation.South;
                case Orientation.South:
                    return Orientation.East;
                case Orientation.East:
                    return Orientation.North;
                default:
                    throw new NotSupportedException("Invalid orientation");
            }
        }

        private Orientation TurnRoverRight(Orientation roverOrientation)
        {
            switch (roverOrientation)
            {
                case Orientation.North:
                    return Orientation.East;
                case Orientation.East:
                    return Orientation.South;
                case Orientation.South:
                    return Orientation.West;
                case Orientation.West:
                    return Orientation.North;
                default:
                    throw new NotSupportedException("Invalid orientation");
            }
        }

        private CoordinatesDto RoverAdvance(CoordinatesDto roverCoordinates, Orientation roverOrientation)
        {
            switch (roverOrientation)
            {
                case Orientation.North:
                    roverCoordinates.Y += 1;
                    break;
                case Orientation.East:
                    roverCoordinates.X += 1;
                    break;
                case Orientation.South:
                    roverCoordinates.Y -= 1;
                    break;
                case Orientation.West:
                    roverCoordinates.X -= 1;
                    break;
                default:
                    throw new NotSupportedException("Invalid advance direction");
            }

            return roverCoordinates;
        }

        private bool IsRoverStillInSquare(CoordinatesDto coordinates, int squareWidth, int squareHeight)
        {
            return coordinates.X >= 0 &&
                   coordinates.Y >= 0 &&
                   coordinates.X <= squareWidth &&
                   coordinates.Y <= squareHeight;
        }
    }
}
