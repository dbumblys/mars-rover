﻿using System;
using System.Collections.Generic;
using System.Text;
using Contracts;
using Entities;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _context;
        private ICommandsRepository _commands;
        private ICoordinatesRepository _coordinates;
        private IRoverRepository _rover;
        private ISquareRepository _square;

        public RepositoryWrapper(RepositoryContext context)
        {
            _context = context;
        }

        public ICommandsRepository Commands
        {
            get { return _commands ??= new CommandsRepository(_context); }
        }

        public ICoordinatesRepository Coordinates
        {
            get { return _coordinates ??= new CoordinatesRepository(_context); }
        }

        public IRoverRepository Rover
        {
            get { return _rover ??= new RoverRepository(_context); }
        }

        public ISquareRepository Square
        {
            get { return _square ??= new SquareRepository(_context); }
        }
    }
}
