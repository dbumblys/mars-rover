﻿using System;
using System.Collections.Generic;
using System.Text;
using Contracts;
using Entities;
using Entities.Models;

namespace Repository
{
    public class CommandsRepository : RepositoryBase<Commands>, ICommandsRepository
    {
        public CommandsRepository(RepositoryContext context) : base(context)
        {
        }
    }
}
