﻿using System;
using System.Collections.Generic;
using System.Text;
using Contracts;
using Entities;
using Entities.Models;

namespace Repository
{
    public class SquareRepository : RepositoryBase<Square>, ISquareRepository
    {
        public SquareRepository(RepositoryContext context) : base(context)
        {
        }
    }
}
