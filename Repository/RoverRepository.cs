﻿using System;
using System.Collections.Generic;
using System.Text;
using Contracts;
using Entities;
using Entities.Models;

namespace Repository
{
    public class RoverRepository : RepositoryBase<Rover>, IRoverRepository
    {
        public RoverRepository(RepositoryContext context) : base(context)
        {
        }
    }
}
