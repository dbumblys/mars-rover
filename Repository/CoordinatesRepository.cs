﻿using System;
using System.Collections.Generic;
using System.Text;
using Contracts;
using Entities;
using Entities.Models;

namespace Repository
{
    public class CoordinatesRepository : RepositoryBase<Coordinates>, ICoordinatesRepository
    {
        public CoordinatesRepository(RepositoryContext context) : base(context)
        {
        }
    }
}
