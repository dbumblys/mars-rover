﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using Services;

namespace MarsRover.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SquareController : ControllerBase
    {
        private readonly RoverService _roverService;
        private readonly SquareService _squareService;
        private readonly CommandsService _commandsService;

        public SquareController(IRepositoryWrapper repository)
        {
            _roverService = new RoverService(repository);
            _squareService = new SquareService(repository);
            _commandsService = new CommandsService(repository);
        }

        [HttpPost]
        public async Task<IActionResult> AddSquare([FromBody] SquareCreateDto square)
        {
            if (square.Width < 0 || square.Height < 0)
                return BadRequest("Invalid square height or width");

            var entity = await _squareService.CreateSquare(square);

            return Created("/Square", entity);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateSquare([FromBody] SquareDto square)
        {
            if (square.Id < 0 || square.Width < 0 || square.Height < 0)
                return BadRequest("Invalid data");

            var entity = await _squareService.GetSquare(square.Id);

            if (entity == null)
                return NotFound();

            entity = await _squareService.UpdateSquare(entity, square.Height, square.Width);

            return Ok(entity);
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetSquare([FromRoute] int id)
        {
            var square = await _squareService.GetSquare(id);

            if (square == null)
                return NotFound();

            return Ok(square);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllSquares()
        {
            var squares = await _squareService.GetAllSquares();

            if (!squares.Any())
                return NotFound();

            return Ok(squares);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSquare([FromRoute] int id)
        {
            var square = await _squareService.GetSquare(id);

            if (square == null)
                return NotFound();

            await _squareService.DeleteSquare(square);

            return NoContent();
        }

        [HttpPost("{id}/Rover")]
        public async Task<IActionResult> AddRoverToSquare([FromRoute] int id, [FromBody] RoverCreateDto rover)
        {
            if (rover.Coordinates.X < 0 || rover.Coordinates.Y < 0 || id < 1)
                return BadRequest("Invalid data");

            await _roverService.CreateRover(rover, id);

            return NoContent();
        }

        [HttpPost("{squareId}/Rover/{roverId}/Commands")]
        public async Task<IActionResult> AddCommandsForRover([FromRoute] int squareId, [FromRoute] int roverId, [FromBody] CommandsDto commands)
        {
            if (string.IsNullOrWhiteSpace(commands.Commands))
                return BadRequest("Invalid data");

            var square = await _squareService.GetSquare(squareId);

            if (square == null)
                return NotFound("Square not found");

            var rover = await _roverService.GetRover(roverId);

            if (rover == null)
                return NotFound("Rover not found");

            await _commandsService.CreateCommand(commands, roverId);

            return NoContent();
        }

        [HttpGet("{squareId}/Rover/{roverId}/Commands/{commandsId}")]
        public async Task<IActionResult> GetResult([FromRoute] int squareId, [FromRoute] int roverId, [FromRoute] int commandsId)
        {
            var rover = await _roverService.GetRoverWithChilds(roverId);

            if (rover == null)
                return NotFound("Rover not found");

            string roverCommands = rover.Commands.FirstOrDefault(x => x.Id == commandsId)?.RoverCommands;

            if (string.IsNullOrWhiteSpace(roverCommands))
                return NotFound("Rover commands not found");

            if (rover.Coordinates.X < 0 || rover.Coordinates.Y < 0)
                return BadRequest("Invalid rover coordinates");

            var roverCoordinates = new CoordinatesDto()
            {
                X = rover.Coordinates.X,
                Y = rover.Coordinates.Y
            };

            ResultDto result;

            try
            {
                result = _roverService.ExecuteRoverCommands(roverCommands, roverCoordinates, (Orientation) rover.Orientation.Id, rover.Square.Width, rover.Square.Height);

                return Ok(result);
            }
            catch (Exception)
            {
                result = new ResultDto() {Coordinates = roverCoordinates, IsValid = false, Orientation = rover.Orientation.Id};

                return BadRequest(result);
            }
        }
    }
}
