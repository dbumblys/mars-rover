﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IRepositoryWrapper
    {
        ICommandsRepository Commands { get; }
        ICoordinatesRepository Coordinates { get; }
        IRoverRepository Rover { get; }
        ISquareRepository Square { get; }
    }
}
